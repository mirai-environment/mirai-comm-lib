package miraiComm

import (
	"github.com/godbus/dbus"
)

// AppletCommunicator define los métodos para la comunicación entre applets
type AppletCommunicator interface {
	SendBroadcastMessage(message string) error
	SendMessageToApplet(appletID string, message string) error
	RegisterApplet(appletID string) error
	UnregisterApplet(appletID string) error
}

// MiraiComm implementa AppletCommunicator usando DBus para la comunicación
type MiraiComm struct {
	connection *dbus.Conn
}

// NewMiraiComm crea una nueva instancia de MiraiComm
func NewMiraiComm() (*MiraiComm, error) {
	conn, err := dbus.SessionBus()
	if err != nil {
		return nil, err
	}
	return &MiraiComm{
		connection: conn,
	}, nil
}

// SendBroadcastMessage envía un mensaje a todos los applets registrados
func (dc *MiraiComm) SendBroadcastMessage(message string) error {
	return dc.connection.Emit("/com/mirai/applet/All", "com.mirai.applet.ReceiveMessage", message)
}

// SendMessageToApplet envía un mensaje a un applet específico
func (dc *MiraiComm) SendMessageToApplet(appletID string, message string) error {
	appletPath := dbus.ObjectPath("/com/mirai/applet/" + appletID)
	return dc.connection.Emit(appletPath, "com.mirai.applet.ReceiveMessage", message)
}

// RegisterApplet registra un nuevo applet en el sistema
func (dc *MiraiComm) RegisterApplet(appletID string) error {
	// Aquí se podría manejar la lógica de registro, como agregar el applet a una lista de control
	return nil
}

// UnregisterApplet elimina un applet del registro
func (dc *MiraiComm) UnregisterApplet(appletID string) error {
	// Aquí se manejaría la eliminación del applet de la lista de control
	return nil
}
