# Applet API Library for Mirai Desktop

This Go library provides functionality to connect and communicate with applets in the Mirai Desktop environment. Mirai Desktop is a desktop environment developed in Go with GTK3 for Linux environments.

## Installation

You can install the library using `go get`:

```sh
go get gitlab.com/mirai-environment/mirai-comm-lib
```

## Usage

```go
package main

import (
    "gitlab.com/mirai-environment/mirai-comm-lib"
    "fmt"
)

func main() {
    // Create a new MiraiComm instance
    communicator, err := miraiComm.NewMiraiComm()
    if err != nil {
        fmt.Println("Error creating communicator:", err)
        return
    }

    // Example usage: Send a broadcast message to all registered applets
    err = communicator.SendBroadcastMessage("Hello, world!")
    if err != nil {
        fmt.Println("Error sending broadcast message:", err)
        return
    }

    // Example usage: Send a message to a specific applet
    err = communicator.SendMessageToApplet("appletID", "Hello, applet!")
    if err != nil {
        fmt.Println("Error sending message to applet:", err)
        return
    }

    // Further usage: Register and unregister applets
    // Note: Registration and unregistration logic should be implemented separately
}
```

## API Reference

### `AppletCommunicator` Interface

This interface defines the methods for communication between applets.

- `SendBroadcastMessage(message string) error`: Sends a message to all registered applets.
- `SendMessageToApplet(appletID string, message string) error`: Sends a message to a specific applet.
- `RegisterApplet(appletID string) error`: Registers a new applet in the system.
- `UnregisterApplet(appletID string) error`: Unregisters an applet from the system.

### `MiraiComm` Struct

This struct implements the `AppletCommunicator` interface using DBus for communication.

#### Methods

- `NewMiraiComm() (*MiraiComm, error)`: Creates a new instance of `MiraiComm`.


## License

GNU GENERAL PUBLIC LICENSE
Version 3, 29 June 2007

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
